package by.epam.training.listener;

import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Nastya Dubovik
 */
public class ContextListener implements ServletContextListener {

    private static final Logger LOG = LogManager.getLogger(ContextListener.class);

    /**
     * Init connection pool
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPoolImpl.getInstance().init();
        } catch (ConnectionPoolException e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * Destroy connection pool
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ConnectionPoolImpl.getInstance().destroy();
    }
}
