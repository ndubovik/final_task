package by.epam.training.domain;


import java.io.Serializable;
import java.util.*;

/**
 * @author Nastya Dubovik
 * Decribes the entity Abiturient
 */
public class Abiturient implements Serializable{

    private static final long serialVersionID = 1L;

    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private String passportData;
    private String city;
    private String street;
    private String house;
    private String flat;
    private double schoolScore;
    private List<SubjectCertificate> tests;
    private boolean available = true;

    public Abiturient() {
        this.id = 0;
        this.firstName = new String();
        this.lastName = new String();
        this.patronymic = new String();
        this.birthDay = new String();
        this.birthMonth = new String();
        this.birthYear = new String();
        this.passportData = new String();
        this.city = new String();
        this.street = new String();
        this.house = new String();
        this.flat = new String();
        this.schoolScore = 0;
        this.tests = new LinkedList<>();
    }
    
    public Abiturient(int id, String firstName, String lastName, String patronymic, String birthDay, String birthMonth, String birthYear, String passportData, String city, String street, String house, String flat, double schoolScore, List<SubjectCertificate> tests) {
        this.id = id;
        this.firstName = new String(firstName);
        this.lastName = new String(lastName);
        this.patronymic = new String(patronymic);
        this.birthDay = new String(birthDay);
        this.birthMonth = new String(birthMonth);
        this.birthYear = new String(birthYear);
        this.passportData = new String(passportData);
        this.city = new String(city);
        this.street = new String(street);
        this.house = new String(house);
        this.flat = new String(flat);
        this.schoolScore = schoolScore;
        this.tests = new LinkedList<>();
        this.tests.addAll(tests);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String formDateOfBirth() {
        StringBuffer sb = new StringBuffer();
        sb.append(birthYear + "-" + birthMonth + "-" + birthDay);
        return sb.toString();
    }


    public String getPassportData() {
        return passportData;
    }

    public void setPassportData(String passportData) {
        this.passportData = passportData;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String formAdress() {
        StringBuffer sb = new StringBuffer();
        sb.append(city+" "+street+" "+house+" "+flat);
        return sb.toString();
    }

    public double getSchoolScore() {
        return schoolScore;
    }

    public void setSchoolScore(double schoolScore) {
        this.schoolScore = schoolScore;
    }

    public List<SubjectCertificate> getTests() {
        return tests;
    }

    public void setTests(List<SubjectCertificate> tests) {
        this.tests.addAll(tests);
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

}
