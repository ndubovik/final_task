package by.epam.training.domain;

/**
 * @author Nastya Dubovik
 * Describes all subjects
 */
public enum Subject  {

    RUSSIAN_LANG(1, "Russian Language" , "\u0420\u0443\u0441\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    BELARUSIAN_LANG(2, "Belarusian Language" , "\u0411\u0435\u043b\u043e\u0440\u0443\u0441\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    MATH(3, "Math" , "\u041c\u0430\u0442\u0435\u043c\u0430\u0442\u0438\u043a\u0430"),
    PHYSICS(4, "Physics" , "\u0424\u0438\u0437\u0438\u043a\u0430"),
    CHEMISTRY(5,"Chemistry" , "\u0425\u0438\u043c\u0438\u044f"),
    BIOLOGY(6, "Biology" , "\u0411\u0438\u043e\u043b\u043e\u0433\u0438\u044f"),
    GERMAN(7, "German" , "\u041d\u0435\u043c\u0435\u0446\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    ENGLISH(8, "English" , "\u0410\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    SPANISH(9, "Spanish" , "\u0418\u0441\u043f\u0430\u043d\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    FRENCH(10, "French" , "\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0438\u0439 \u044f\u0437\u044b\u043a"),
    HISTORY_OF_BELARUS(11, "History_Of_Belarus" , "\u0418\u0441\u0442\u043e\u0440\u0438\u044f \u0411\u0435\u043b\u0430\u0440\u0443\u0441\u0438"),
    WORLD_HISTORY(12, "World_History" , "\u0412\u0441\u0435\u043c\u0438\u0440\u043d\u0430\u044f \u0438\u0441\u0442\u043e\u0440\u0438\u044f"),
    CIVICS(13, "Civics" , "\u041e\u0431\u0449\u0435\u0441\u0442\u0432\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u0435"),
    GEOGRAPHY(14, "Geography" , "\u0413\u0435\u043e\u0433\u0440\u0430\u0444\u0438\u044f");

    private int code;
    private String englishName;
    private String russianName;

    Subject(int code, String englishName, String russianName){
        this.code = code;
        this.russianName = russianName;
        this.englishName = englishName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getRussianName() {
        return russianName.toString();
    }

    public void setRussianName(String russianName) {
        this.russianName = russianName;
    }

    /**
     * Get subject by its code
     * @param code
     * @return Subject
     * if such subject exist else null
     */
    public static Subject getSubject(int code){
        switch(code){
            case 1: return RUSSIAN_LANG;
            case 2: return BELARUSIAN_LANG;
            case 3: return MATH;
            case 4: return PHYSICS;
            case 5: return CHEMISTRY;
            case 6: return BIOLOGY;
            case 7: return GERMAN;
            case 8: return ENGLISH;
            case 9: return SPANISH;
            case 10: return FRENCH;
            case 11: return HISTORY_OF_BELARUS;
            case 12: return WORLD_HISTORY;
            case 13: return CIVICS;
            case 14: return GEOGRAPHY;
            default: return null;
        }
    }
}
