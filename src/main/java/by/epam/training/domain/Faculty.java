package by.epam.training.domain;

import java.io.Serializable;

/**
 * @author Nastya Dubovik
 * Describes the entity of Faculty
 */
public class Faculty implements Serializable {

    private static final long serialVersionID = 1L;

    private int id;
    private String name;
    private int previousPassingScore;
    private int plan;
    private int abiturientAmount;

    public Faculty(String name, int previousPassingScore, int plan) {
        this.id = 0;
        this.name = name;
        this.previousPassingScore = previousPassingScore;
        this.plan = plan;
        this.abiturientAmount = 0;
    }

    public Faculty(String name, int previousPassingScore, int plan, int abiturientAmount) {
        this.id = 0;
        this.name = name;
        this.previousPassingScore = previousPassingScore;
        this.plan = plan;
        this.abiturientAmount = abiturientAmount;
    }

    public Faculty(int id, String name, int previousPassingScore, int plan) {
        this.id = id;
        this.name = name;
        this.previousPassingScore = previousPassingScore;
        this.plan = plan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPreviousPassingScore() {
        return previousPassingScore;
    }

    public void setPreviousPassingScore(int previousPassingScore) {
        this.previousPassingScore = previousPassingScore;
    }

    public int getPlan() {
        return plan;
    }

    public void setPlan(int plan) {
        this.plan = plan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAbiturientAmount() {
        return abiturientAmount;
    }

    public void setAbiturientAmount(int abiturientAmount) {
        this.abiturientAmount = abiturientAmount;
    }
}
