package by.epam.training.domain;

import java.io.Serializable;

/**
 * @author Nastya Dubovik
 * Describes the entity of subject certificate
 */
public class SubjectCertificate implements Serializable {

    private static final long serialVersionID = 1L;

    private int id;
    private int score;
    private int abiturientId;
    private Subject subject;

    public SubjectCertificate(int id, int score, int abiturientId, Subject subject) {
        this.id = id;
        this.score = score;
        this.abiturientId = abiturientId;
        this.subject = subject;
    }

    public SubjectCertificate(int score, Subject subject) {
        this.id = 0;
        this.score = score;
        this.abiturientId = 0;
        this.subject = subject;
    }

    public SubjectCertificate(int abiturientId, int score, Subject subject) {
        this.id = 0;
        this.score = score;
        this.abiturientId = abiturientId;
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getAbiturientId() {
        return abiturientId;
    }

    public void setAbiturientId(int abiturientId) {
        this.abiturientId = abiturientId;
    }

    public String getStringSubject() {
        return subject.toString();
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public void setSubject(String subject) {
        this.subject = Subject.valueOf(subject.toUpperCase());
    }
}
