package by.epam.training.domain;

import java.io.Serializable;

/**
 * @author Nastya Dubovik
 * Describes the entity of user
 */
public class User  implements Serializable{

    private static final long serialVersionID = 1L;

    private int id;
    private String login;
    private String password;
    private boolean available = true;

    public User(String login, String password) {
        this.id = 0;
        this.login = login;
        this.password = password;
    }

    public User(int id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

}
