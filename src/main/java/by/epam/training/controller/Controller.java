package by.epam.training.controller;

import by.epam.training.controller.command.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nastya Dubovik
 * Controller receive request from client (get or post).
 * @see javax.servlet.http.HttpServlet
 * @see by.epam.training.controller.ControllerHelper
 */
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LogManager.getLogger(Controller.class);

	private static final String PARAMETER_ACTION = "action";
	private static final String PARAMETER_PAGE = "page";
	private static final String PARAMETER_ERROR = "error";
	private static final String ERROR_PAGE = "/index.jsp";

    private ControllerHelper helper = new ControllerHelper();
	
	
    public Controller() {
        super();
    }

	/**
	 *
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Receive post request or request that redirected from doGet,
	 * then with the help of ControllerHelper define command and execute it.
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goTo = null;
		try {
			goTo = helper.getCommand(request.getParameter(PARAMETER_ACTION)).execute(request);
			request.getRequestDispatcher(goTo).forward(request, response);
		} catch (CommandException e) {
			LOG.error("Error in controller: " + e.getMessage());
			request.setAttribute(PARAMETER_ERROR, e.getMessage());
			String errorPage = request.getParameter(PARAMETER_PAGE);
			request.getRequestDispatcher(errorPage).forward( request, response);
		} catch (Exception e){
			LOG.error("Error in controller(exception): "+e.getMessage());
			request.setAttribute(PARAMETER_ERROR, e.getMessage());
			request.getRequestDispatcher(ERROR_PAGE).forward(request, response);
		}
	}

}
