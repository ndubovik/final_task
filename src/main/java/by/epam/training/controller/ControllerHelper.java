package by.epam.training.controller;


import by.epam.training.controller.command.Command;
import by.epam.training.controller.command.impl.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nastya Dubovik
 * ControllerHelper is used for defining command.
 * @see by.epam.training.controller.Controller
 */
public class ControllerHelper {
	private Map<CommandName, Command> commands = new HashMap<>();
	
	public ControllerHelper(){
		commands.put(CommandName.LOGIN, new LoginCommandImpl());
		commands.put(CommandName.LOGOUT, new LogoutCommandImpl());
		commands.put(CommandName.REGISTER, new RegistrationUserCommandImpl());
		commands.put(CommandName.EN, new ChangeLocaleCommandImpl());
		commands.put(CommandName.RU, new ChangeLocaleCommandImpl());
		commands.put(CommandName.FORM, new GetFormCommandImpl());
		commands.put(CommandName.EDIT_FORM, new EditFormCommandImpl());
		commands.put(CommandName.APPLY, new ApplyCommandImpl());
		commands.put(CommandName.MAIN, new MainCommandImpl());
		commands.put(CommandName.GO_TO_EDIT_FORM, new GoToEditFormCommandImpl());
		commands.put(CommandName.MAIL, new MailCommandImpl());
		commands.put(CommandName.PICK_UP, new PickUpApplicationCommandImpl());
	}

	/**
	 *
	 * @param commandName
	 * command that come from client
	 * @return Command
	 * command that should be execute
	 */
	public Command getCommand(String commandName){
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		return commands.get(name);
	}
	
	
	enum CommandName{
		LOGIN, LOGOUT, REGISTER, EN, RU, FORM, EDIT_FORM, APPLY, MAIN, GO_TO_EDIT_FORM, MAIL, PICK_UP;
	}
}
