package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.CommandException;
import by.epam.training.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class LogoutCommandImpl implements Command {

    private static final String TO_GO="/index.jsp";

    /**
     * Describe command that logout user.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession(false).invalidate();
        return TO_GO;
    }

}
