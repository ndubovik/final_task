package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.CommandException;
import by.epam.training.controller.command.Command;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.RegisterServiceImpl;
import by.epam.training.service.impl.UniversityServiceImpl;
import by.epam.training.validation.AbiturientValidation;
import by.epam.training.validation.UserValidation;
import by.epam.training.validation.ValidationException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class RegistrationUserCommandImpl implements Command {

    private static final String PARAMETER_LOGIN="login";
    private static final String PARAMETER_PASSWORD="password";

    private static final String TO_GO="/WEB-INF/jsp/main.jsp";

    /**
     * Describe command that at first validate parameters and execute
     * service that register user.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     * @see by.epam.training.service.ServiceException
     * @see by.epam.training.validation.ValidationException
     * @see by.epam.training.service.impl.RegisterServiceImpl
     * @see by.epam.training.service.impl.UniversityServiceImpl
     * @see by.epam.training.validation.UserValidation
     * @see by.epam.training.validation.AbiturientValidation
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String login = request.getParameter(PARAMETER_LOGIN);
        String password = request.getParameter(PARAMETER_PASSWORD);
        try{
            UserValidation.getInstance().validate(login, password);
            AbiturientValidation.getInstance().validate(request);

            RegisterServiceImpl.getInstance().doService(request);
            UniversityServiceImpl.getInstance().doService(request);

        } catch (ValidationException | ServiceException e){
            throw new CommandException(e);
        }

        return TO_GO;
    }



}
