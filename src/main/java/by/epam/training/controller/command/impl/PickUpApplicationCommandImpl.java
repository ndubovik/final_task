package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.Command;
import by.epam.training.controller.command.CommandException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.MailServiceImpl;
import by.epam.training.service.impl.PickUpApplicationServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class PickUpApplicationCommandImpl implements Command {

    private static final String TO_GO="/WEB-INF/jsp/mail.jsp";

    /**
     * Describe command that execute service that pick up
     * documents from faculty.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     * @see by.epam.training.service.ServiceException
     * @see by.epam.training.service.impl.MailServiceImpl
     * @see by.epam.training.service.impl.PickUpApplicationServiceImpl
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        try {
            PickUpApplicationServiceImpl.getInstance().doService(request);
            MailServiceImpl.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }


        return TO_GO;
    }

}
