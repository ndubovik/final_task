package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.Command;
import by.epam.training.controller.command.CommandException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.EditFormServiceImpl;
import by.epam.training.validation.AbiturientValidation;
import by.epam.training.validation.ValidationException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class EditFormCommandImpl implements Command {

    private static final String TO_GO="/WEB-INF/jsp/form.jsp";

    /**
     * Describe command that at first validate parameters from request
     * and then execute service that edit form.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     * @see by.epam.training.service.ServiceException
     * @see by.epam.training.validation.ValidationException
     * @see by.epam.training.service.impl.EditFormServiceImpl
     * @see by.epam.training.validation.AbiturientValidation
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        try{

            AbiturientValidation.getInstance().validate(request);
            EditFormServiceImpl.getInstance().doService(request);

        } catch(ValidationException | ServiceException e) {
            throw new CommandException(e);
        }

        return TO_GO;

    }

}
