package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.Command;
import by.epam.training.controller.command.CommandException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.UniversityServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class MainCommandImpl implements Command {

    private static final String TO_GO="/WEB-INF/jsp/main.jsp";

    /**
     * Describe command that execute service that get main
     * page after clicking on menu or login.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     * @see by.epam.training.service.ServiceException
     * @see by.epam.training.service.impl.UniversityServiceImpl
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        try {
            UniversityServiceImpl.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return TO_GO;
    }

}
