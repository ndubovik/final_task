package by.epam.training.controller.command.impl;


import by.epam.training.controller.command.CommandException;
import by.epam.training.controller.command.Command;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.LoginServiceImpl;
import by.epam.training.service.impl.UniversityServiceImpl;
import by.epam.training.validation.UserValidation;
import by.epam.training.validation.ValidationException;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class LoginCommandImpl implements Command {

	private static final String PARAMETR_LOGIN="login";
	private static final String PARAMETR_PASSWORD="password";
	private static final String TO_GO="/WEB-INF/jsp/main.jsp";

	/**
	 * Describe command that at first validate parameters and
	 * then execute service that login user and show faculties.
	 * @param request
	 * @return String
	 * the url of page to go after execution
	 * @throws CommandException
	 * @see by.epam.training.controller.command.CommandException
	 * @see by.epam.training.service.ServiceException
	 * @see by.epam.training.validation.ValidationException
	 * @see by.epam.training.service.impl.LoginServiceImpl
	 * @see by.epam.training.service.impl.UniversityServiceImpl
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String login = request.getParameter(PARAMETR_LOGIN);
		String password = request.getParameter(PARAMETR_PASSWORD);
		try {

			UserValidation.getInstance().validate(login, password);
			LoginServiceImpl.getInstance().doService(request);
			UniversityServiceImpl.getInstance().doService(request);

		} catch (ValidationException | ServiceException e){
			throw new CommandException(e);
		}

		return TO_GO;

	}

}
