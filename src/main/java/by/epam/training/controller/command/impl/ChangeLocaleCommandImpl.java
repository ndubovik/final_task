package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.CommandException;
import by.epam.training.controller.command.Command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class ChangeLocaleCommandImpl implements Command {

    private static final String PARAMETER_ACTION="action";
    private static final String PARAMETER_PAGE="page";
    private static final String ATTR_LOCALE = "locale";

    /**
     * Describe command that change locale.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String locale = request.getParameter(PARAMETER_ACTION).toLowerCase();
        request.getSession(false).setAttribute(ATTR_LOCALE, locale);
        String goTo = request.getParameter(PARAMETER_PAGE);
        return goTo;
    }
}
