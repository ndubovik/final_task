package by.epam.training.controller.command;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Nastya Dubovik
 */
public interface Command {
	/**
	 *
	 * @param request
	 * that come from Controller
	 * @return String
	 * the url of page to go after execution
	 * @throws CommandException
	 * @see by.epam.training.controller.command.CommandException
	 */
	String execute(HttpServletRequest request) throws CommandException;
}
