package by.epam.training.controller.command.impl;

import by.epam.training.controller.command.Command;
import by.epam.training.controller.command.CommandException;
import by.epam.training.service.ServiceException;
import by.epam.training.service.impl.ApplyServiceImpl;
import by.epam.training.service.impl.UniversityServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public class ApplyCommandImpl implements Command {

    private static final String TO_GO="/WEB-INF/jsp/main.jsp";

    /**
     * Describe command that execute service that apply documents to faculty.
     * @param request
     * @return String
     * the url of page to go after execution
     * @throws CommandException
     * @see by.epam.training.controller.command.CommandException
     * @see by.epam.training.service.ServiceException
     * @see by.epam.training.service.impl.ApplyServiceImpl
     * @see by.epam.training.service.impl.UniversityServiceImpl
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        try {
            ApplyServiceImpl.getInstance().doService(request);
            UniversityServiceImpl.getInstance().doService(request);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }


        return TO_GO;
    }

}
