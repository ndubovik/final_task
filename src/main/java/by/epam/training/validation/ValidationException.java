package by.epam.training.validation;

/**
 * @author Nastya Dubovik
 * Describes exception in validation
 */
public class ValidationException extends Exception {
    public ValidationException(){}
    public ValidationException(String message, Throwable exception) {
        super(message, exception);
    }
    public ValidationException(String message) {
        super(message);
    }
    public ValidationException(Throwable exception) {
        super(exception);
    }
}
