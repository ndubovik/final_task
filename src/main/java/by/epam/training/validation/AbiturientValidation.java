package by.epam.training.validation;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nastya Dubovik
 * Validate abiturient parameters
 */
public class AbiturientValidation {

    private static final String CHARACTER_PATTERN = "^[a-zA-Z�-��-�]+$";
    private static final String NUMBER_PATTERN = "^[0-9]+$";
    private static final String DATE_PATTERN = "^[0-9]{2,4}$";
    private static final String PASSPORT_PATTERN = "^[a-zA-Z]{2}[0-9]{7}$";
    private static final String SCHOOL_SCORE_PATTERN = "^[1-9]\\.[0-9]$";
    private static final String SCHOOL_SCORE_PATTERN_WITHOUT_DOT = "(^[1-9]$)|10";
    private static final String TEST_SCORE_PATTERN = "(^[0-9]{1,2}$)|100";

    private static final Pattern character_pattern = Pattern.compile(CHARACTER_PATTERN);
    private static final Pattern date_pattern = Pattern.compile(DATE_PATTERN);
    private static final Pattern number_pattern = Pattern.compile(NUMBER_PATTERN);
    private static final Pattern passport_pattern = Pattern.compile(PASSPORT_PATTERN);
    private static final Pattern school_score_pattern = Pattern.compile(SCHOOL_SCORE_PATTERN);
    private static final Pattern school_score_pattern_without_dot = Pattern.compile(SCHOOL_SCORE_PATTERN_WITHOUT_DOT);
    private static final Pattern test_score_pattern = Pattern.compile(TEST_SCORE_PATTERN);

    private static final String PARAMETR_FIRST_NAME = "first_name";
    private static final String PARAMETR_LAST_NAME = "last_name";
    private static final String PARAMETR_PATRONYMIC = "patronymic";
    private static final String PARAMETR_DAY_OF_BIRTH = "day_of_birth";
    private static final String PARAMETR_MONTH_OF_BIRTH = "month_of_birth";
    private static final String PARAMETR_YEAR_OF_BIRTH = "year_of_birth";
    private static final String PARAMETR_PASSSPORT_DATA = "passport_data";
    private static final String PARAMETR_ADRESS_CITY= "city";
    private static final String PARAMETR_ADRESS_STREET= "street";
    private static final String PARAMETR_ADRESS_HOUSE= "house";
    private static final String PARAMETR_ADRESS_FLAT= "flat";
    private static final String PARAMETR_SCHOOL_SCORE= "school_score";
    private static final String PARAMETR_TEST1= "test1";
    private static final String PARAMETR_TEST2= "test2";
    private static final String PARAMETR_TEST3= "test3";
    private static final String PARAMETR_SUBJECT1 = "subject1";
    private static final String PARAMETR_SUBJECT2 = "subject2";
    private static final String PARAMETR_SUBJECT3 = "subject3";

    private static final String PARAMETR_LOGIN="login";
    private static final String PARAMETR_PASSWORD="password";

    private Matcher matcher;

    private static final AbiturientValidation instance = new AbiturientValidation();

    /**
     *
     * @return AbiturientValidation
     * the instance of this class
     */
    public static AbiturientValidation getInstance(){
        return instance;
    }

    /**
     *
     * @param request
     * @throws ValidationException
     */
    public void validate(HttpServletRequest request) throws ValidationException {
        Map<String, String> paramList = prepareToValidation(request);
        try {
            String first_name = new String(paramList.get(PARAMETR_FIRST_NAME).trim().getBytes(), "utf-8");
            matcher = character_pattern.matcher(first_name);
            if( !matcher.matches()){
                throw new ValidationException("Exception in first name validation.");
            }

            String last_name = new String(paramList.get(PARAMETR_LAST_NAME).trim().getBytes(), "utf-8");
            matcher = character_pattern.matcher(last_name);
            if( !matcher.matches()){
                throw new ValidationException("Exception in last name validation.");
            }

            String patronymic = new String(paramList.get(PARAMETR_PATRONYMIC).trim().getBytes(), "utf-8");
            matcher = character_pattern.matcher(patronymic);
            if( !matcher.matches()){
                throw new ValidationException("Exception in patronymic validation.");
            }

            String day_of_birth = new String(paramList.get(PARAMETR_DAY_OF_BIRTH).trim().getBytes(), "utf-8");
            matcher = date_pattern.matcher(day_of_birth);
            if( !matcher.matches()){
                throw new ValidationException("Exception in day of birth validation.");
            }

            String month_of_birth = new String(paramList.get(PARAMETR_MONTH_OF_BIRTH).trim().getBytes(), "utf-8");
            matcher = date_pattern.matcher(month_of_birth);
            if( !matcher.matches()){
                throw new ValidationException("Exception in month of birth validation.");
            }

            String year_of_birth = new String(paramList.get(PARAMETR_YEAR_OF_BIRTH).trim().getBytes(), "utf-8");
            matcher = date_pattern.matcher(year_of_birth);
            if( !matcher.matches()){
                throw new ValidationException("Exception in year of birth validation.");
            }

            String passport_data = new String(paramList.get(PARAMETR_PASSSPORT_DATA).trim().getBytes(), "utf-8");
            matcher = passport_pattern.matcher(passport_data);
            if( !matcher.matches()){
                throw new ValidationException("Exception in passport data validation.");
            }

            String city = new String(paramList.get(PARAMETR_ADRESS_CITY).trim().getBytes(), "utf-8");
            matcher = character_pattern.matcher(city);
            if( !matcher.matches()){
                System.out.println("error in city: "+city);
                throw new ValidationException("Exception in city validation.");
            }

            String street = new String(paramList.get(PARAMETR_ADRESS_STREET).trim().getBytes(), "utf-8");
            matcher = character_pattern.matcher(street);
            if( !matcher.matches()){
                throw new ValidationException("Exception in street validation.");
            }

            String house = new String(paramList.get(PARAMETR_ADRESS_HOUSE).trim().getBytes(), "utf-8");
            matcher = number_pattern.matcher(house);
            if( !matcher.matches()){
                throw new ValidationException("Exception in house validation.");
            }

            String flat = new String(paramList.get(PARAMETR_ADRESS_FLAT).trim().getBytes(), "utf-8");
            matcher = number_pattern.matcher(flat);
            if( !matcher.matches()){
                System.out.println("error in flat: "+flat);
                throw new ValidationException("Exception in flat validation.");
            }

            String school_score = new String(paramList.get(PARAMETR_SCHOOL_SCORE).trim().getBytes(), "utf-8");
            matcher = school_score_pattern.matcher(school_score);
            if( !matcher.matches()){
                matcher = school_score_pattern_without_dot.matcher(school_score);
                if( !matcher.matches()){
                    throw new ValidationException("Exception in school score validation.");
                }
            }

            String test1 = new String(paramList.get(PARAMETR_TEST1).trim().getBytes(), "utf-8");
            matcher = test_score_pattern.matcher(test1);
            if( !matcher.matches()){
                throw new ValidationException("Exception in test1 validation.");
            }

            String test2 = new String(paramList.get(PARAMETR_TEST2).trim().getBytes(), "utf-8");
            matcher = test_score_pattern.matcher(test2);
            if( !matcher.matches()){
                throw new ValidationException("Exception in test2 validation.");
            }

            String test3 = new String(paramList.get(PARAMETR_TEST3).trim().getBytes(), "utf-8");
            matcher = test_score_pattern.matcher(test3);
            if( !matcher.matches()){
                throw new ValidationException("Exception in test3 validation.");
            }


            Set<String> tests = new HashSet<>();
            tests.add(paramList.get(PARAMETR_SUBJECT1));
            tests.add(paramList.get(PARAMETR_SUBJECT2));
            tests.add(paramList.get(PARAMETR_SUBJECT3));

            if(tests.size()!=3){
                throw new ValidationException("Exception in subjects validation.");
            }

        } catch (UnsupportedEncodingException e) {
            throw new ValidationException("Exception in encoding.");
        }

    }

    /**
     * Create map that contains all necessary fields to create an abiturient
     * @param request
     * @return Map<String, String>
     */
    private Map<String, String> prepareToValidation(HttpServletRequest request){
        Enumeration<String> parameters = request.getParameterNames();
        String param = null;
        Map<String, String> paramList = new HashMap<>();
        while(parameters.hasMoreElements()){
            param = parameters.nextElement();
            if ( !param.equals(PARAMETR_LOGIN) && !param.equals(PARAMETR_PASSWORD) ){
                paramList.put(param, request.getParameter(param) );
            }
        }
        return paramList;
    }
}
