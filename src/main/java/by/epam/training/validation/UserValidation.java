package by.epam.training.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nastya Dubovik
 * Validate user parameters
 */
public class UserValidation {

    private static final String LOGIN_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String PASSWORD_PATTERN = "^[a-z0-9]{3,15}$";

    private static final Pattern login_pattern = Pattern.compile(LOGIN_PATTERN);
    private static final Pattern password_pattern = Pattern.compile(PASSWORD_PATTERN);
    private Matcher matcher;



    public UserValidation() {}

    private static final UserValidation loginValidation = new UserValidation();

    /**
     *
     * @return UserValidation
     * the instance of this class
     */
    public static UserValidation getInstance(){
        return loginValidation;
    }

    /**
     *
     * @param login
     * to validate
     * @param password
     * to validate
     * @return boolean
     * true if everything go ok, else false
     * @throws ValidationException
     */
    public boolean validate(String login, String password) throws ValidationException {

        matcher = login_pattern.matcher(login);
        if( !matcher.matches()){
            throw new ValidationException("Exception in login validation.");
        }
        matcher = password_pattern.matcher(password);
        if( !matcher.matches()){
            throw new ValidationException("Exception in password validation.");
        }

        return true;

    }
}
