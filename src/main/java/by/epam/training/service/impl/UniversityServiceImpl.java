package by.epam.training.service.impl;

import by.epam.training.dao.*;
import by.epam.training.dao.impl.SQLFacultyDAO;
import by.epam.training.dao.impl.SQLFacultySubjectConnectionDAO;
import by.epam.training.dao.impl.SQLRegisterDAO;
import by.epam.training.dao.impl.SQLSubjectCertificateDAO;
import by.epam.training.domain.Faculty;
import by.epam.training.domain.SubjectCertificate;
import by.epam.training.domain.User;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Nastya Dubovik
 */
public class UniversityServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(UniversityServiceImpl.class);

    private static final UniversityServiceImpl instance = new UniversityServiceImpl();

    private static final String ATTR_FACULTIES = "faculties";
    private static final String PARAMETER_USER = "user";

    /**
     *
     * @return UniversityServiceImpl
     * the instance of this class
     */
    public static UniversityServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to get information
     * about faculties that user can apply to.
     * Service work with database.
     * @param request
     * from Command
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            User user = (User)request.getSession(false).getAttribute(PARAMETER_USER);


            SubjectCertificateDAO subjectCertificateDAO = SQLSubjectCertificateDAO.getInstance();
            List<SubjectCertificate> tests = subjectCertificateDAO.findByAbiturient(user.getId());

            FacultySubjectConnectionDAO facultySubjectConnectionDAO = SQLFacultySubjectConnectionDAO.getInstance();
            FacultyDAO facultyDAO = SQLFacultyDAO.getInstance();
            RegisterDAO registerDAO = SQLRegisterDAO.getInstance();

            List<Integer> applyFacultiesId = registerDAO.findFacultiesByAbiturient(user.getId());

            List<Integer> facultiesId = null;
            Set<Integer> setId = new HashSet<>();
            int i=0;

            for(SubjectCertificate sc : tests){

                facultiesId = facultySubjectConnectionDAO.find(sc.getSubject().getCode());
                if(i==0){
                    setId.addAll(facultiesId);
                } else {
                    setId.retainAll(facultiesId);
                }
                i++;
            }


            if(!applyFacultiesId.isEmpty()){
                setId.removeAll(applyFacultiesId);
                user.setAvailable(false);
            }

            List<Faculty> facultyList = new LinkedList<>();

            for(Integer id : setId){
                Faculty faculty = facultyDAO.find(id);
                int abiturientAmount = registerDAO.findAbiturientsByFaculty(id).size();
                faculty.setAbiturientAmount(abiturientAmount);
                facultyList.add(faculty);
            }

            request.setAttribute(ATTR_FACULTIES, facultyList);

        } catch (DAOException e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }
}
