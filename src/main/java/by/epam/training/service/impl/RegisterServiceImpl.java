package by.epam.training.service.impl;

import by.epam.training.dao.AbiturientDAO;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.UserDAO;
import by.epam.training.dao.impl.*;
import by.epam.training.domain.Abiturient;
import by.epam.training.domain.User;
import by.epam.training.service.utils.InitAbiturient;
import by.epam.training.service.utils.InitUser;
import by.epam.training.service.utils.PasswordHashCode;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Nastya Dubovik
 */
public class RegisterServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(RegisterServiceImpl.class);

    private static final RegisterServiceImpl instance = new RegisterServiceImpl();

    private static final String ATTR_USER = "user";

    /**
     *
     * @return RegisterServiceImpl
     * the instance of this class
     */
    public static RegisterServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to register user.
     * Service work with database.
     * @param request
     * from Command, the request is validated
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            User user = InitUser.init(request);

            String password = PasswordHashCode.getHash(user.getPassword());
            user.setPassword(password);

            AbiturientDAO abiturientDAO = SQLAbiturientDAO.getInstance();
            UserDAO userDAO = SQLUserDAO.getInstance();

            Integer id = userDAO.findId(user);

            if (id==null) {

                userDAO.create(user);
                id = userDAO.findId(user);
                user.setId(id);

                request.getSession(true).setAttribute(ATTR_USER, user);

                Abiturient abiturient = InitAbiturient.init(request);

                abiturientDAO.create(abiturient);

            } else {
                throw new ServiceException("We have such user. Login is not free.");
            }


        } catch (DAOException | NoSuchAlgorithmException e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }
}
