package by.epam.training.service.impl;

import by.epam.training.dao.AbiturientDAO;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.RegisterDAO;
import by.epam.training.dao.impl.SQLAbiturientDAO;
import by.epam.training.dao.impl.SQLRegisterDAO;
import by.epam.training.domain.Abiturient;
import by.epam.training.domain.User;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Nastya Dubovik
 */
public class GetFormServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(GetFormServiceImpl.class);

    private static final GetFormServiceImpl instance = new GetFormServiceImpl();

    private static final String PARAMETER_USER = "user";
    private static final String ATTR_ABITURIENT = "abiturient";

    /**
     *
     * @return GetFormServiceImpl
     * the instance of this class
     */
    public static GetFormServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to get abiturient form t user.
     * Service work with database.
     * @param request
     * from Command, the request is validated
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            User user = (User)request.getSession(false).getAttribute(PARAMETER_USER);

            AbiturientDAO abiturientDAO = SQLAbiturientDAO.getInstance();
            Abiturient abiturient = abiturientDAO.find(user.getId());

            RegisterDAO registerDAO = SQLRegisterDAO.getInstance();


            if (abiturient != null) {

                List<Integer> abiturientId = registerDAO.findAllAbiturients();
                if(abiturientId.contains(user.getId())){
                    abiturient.setAvailable(false);
                }

                request.setAttribute(ATTR_ABITURIENT, abiturient);
            } else {
                throw new ServiceException("We don't have such abiturient.");
            }

        } catch (DAOException e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }

}
