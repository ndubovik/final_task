package by.epam.training.service.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.FacultyDAO;
import by.epam.training.dao.RegisterDAO;
import by.epam.training.dao.impl.SQLFacultyDAO;
import by.epam.training.dao.impl.SQLRegisterDAO;
import by.epam.training.domain.Faculty;
import by.epam.training.domain.User;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nastya Dubovik
 */
public class MailServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(MailServiceImpl.class);

    private static final String ATTR_USER = "user";
    private static final String ATTR_FACULTIES = "faculties";

    private static final MailServiceImpl instance = new MailServiceImpl();

    /**
     *
     * @return MailServiceImpl
     * the instance of this class
     */
    public static MailServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to get information about faculties,
     * that user applied to.
     * Service work with database.
     * @param request
     * from Command
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            User user = (User)request.getSession(false).getAttribute(ATTR_USER);
            Integer userId = user.getId();

            RegisterDAO registerDAO = SQLRegisterDAO.getInstance();
            FacultyDAO facultyDAO = SQLFacultyDAO.getInstance();

            List<Integer> facultiesId = registerDAO.findFacultiesByAbiturient(userId);
            List<Faculty> facultiesList = new LinkedList<>();
            for(Integer id : facultiesId){
                Faculty faculty = facultyDAO.find(id);
                int abiturientAmount = registerDAO.findAbiturientsByFaculty(id).size();
                faculty.setAbiturientAmount(abiturientAmount);
                facultiesList.add(faculty);
            }

            request.setAttribute(ATTR_FACULTIES, facultiesList);

        } catch (DAOException | NumberFormatException  e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            throw new ServiceException(e);
        }

    }
}
