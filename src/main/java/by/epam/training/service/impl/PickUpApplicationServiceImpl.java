package by.epam.training.service.impl;

import by.epam.training.dao.*;
import by.epam.training.dao.impl.SQLRegisterDAO;
import by.epam.training.domain.User;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author Nastya Dubovik
 */
public class PickUpApplicationServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(PickUpApplicationServiceImpl.class);

    private static final PickUpApplicationServiceImpl instance = new PickUpApplicationServiceImpl();

    private static final String PARAMETER_PICK_UP_FROM = "pick_up_from";
    private static final String PARAMETER_USER = "user";
    private static final String PARAMETER_USER_ID="user_id";
    private static final String PARAMETER_FACULTY_ID="faculty_id";

    /**
     *
     * @return PickUpApplicationServiceImpl
     * the instance of this class
     */
    public static PickUpApplicationServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to pick up documents from faculty.
     * Service work with database.
     * @param request
     * from Command
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            User user = (User)request.getSession(false).getAttribute(PARAMETER_USER);
            Integer facultyId = Integer.parseInt(request.getParameter(PARAMETER_PICK_UP_FROM));

            RegisterDAO registerDAO = SQLRegisterDAO.getInstance();
            Map<String, Integer> mapId = new HashMap<>();
            mapId.put(PARAMETER_USER_ID, user.getId());
            mapId.put(PARAMETER_FACULTY_ID, facultyId);
            registerDAO.delete( mapId );

            user.setAvailable(true);

        } catch (DAOException | NumberFormatException e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }
}
