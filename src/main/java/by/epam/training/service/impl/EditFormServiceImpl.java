package by.epam.training.service.impl;

import by.epam.training.dao.AbiturientDAO;
import by.epam.training.dao.impl.SQLAbiturientDAO;
import by.epam.training.domain.Abiturient;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import by.epam.training.service.utils.InitAbiturient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Nastya Dubovik
 */
public class EditFormServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(EditFormServiceImpl.class);

    private static final EditFormServiceImpl instance = new EditFormServiceImpl();

    private static final String ATTR_ABITURIENT = "abiturient";

    /**
     *
     * @return EditFormServiceImpl
     * the instance of this class
     */
    public static EditFormServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main service task is to edit abiturient form.
     * Service work with database.
     * @param request
     * from Command, the request is validated
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            AbiturientDAO abiturientDAO = SQLAbiturientDAO.getInstance();
            Abiturient abiturient = InitAbiturient.init(request);

            boolean abiturientUpdateStatus = abiturientDAO.update(abiturient);

            if (abiturientUpdateStatus) {
                request.setAttribute(ATTR_ABITURIENT, abiturient);
            } else {
                throw new ServiceException("Abiturient is not update.");
            }

        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }

}
