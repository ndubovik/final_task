package by.epam.training.service.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.UserDAO;
import by.epam.training.dao.impl.SQLUserDAO;
import by.epam.training.domain.User;
import by.epam.training.service.utils.PasswordHashCode;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Nastya Dubovik
 */
public class LoginServiceImpl implements Service {

	private static final Logger LOG = LogManager.getLogger(LoginServiceImpl.class);

	private static final LoginServiceImpl instance = new LoginServiceImpl();

	private static final String PARAMETR_LOGIN="login";
	private static final String PARAMETR_PASSWORD="password";
	private static final String ATTR_USER = "user";

	/**
	 *
	 * @return LoginServiceImpl
	 * the instance of this class
	 */
	public static LoginServiceImpl getInstance(){
		return instance;
	}

	/**
	 * The main service task is to login user in system.
	 * Service work with database.
	 * @param request
	 * from Command, the request is validated
	 * @throws ServiceException
	 */
	@Override
	public void doService(HttpServletRequest request) throws ServiceException {
		try {
			String login = request.getParameter(PARAMETR_LOGIN);
			String password = PasswordHashCode.getHash(request.getParameter(PARAMETR_PASSWORD));
			User user = new User(login, password);

			UserDAO userDAO = SQLUserDAO.getInstance();

			Integer id = userDAO.findId(user);

			if (id != null) {
				user.setId(id);
				request.getSession(true).setAttribute(ATTR_USER, user);
			} else {
				throw new ServiceException("We don't have such user.");
			}

		} catch (NoSuchAlgorithmException e) {
			LOG.error(e.getMessage());
			throw new ServiceException(e);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
			throw new ServiceException(e);
		}

	}

}
