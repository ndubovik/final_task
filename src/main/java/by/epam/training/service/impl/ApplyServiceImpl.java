package by.epam.training.service.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.RegisterDAO;
import by.epam.training.dao.impl.SQLRegisterDAO;
import by.epam.training.domain.User;
import by.epam.training.service.Service;
import by.epam.training.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nastya Dubovik
 */
public class ApplyServiceImpl implements Service {

    private static final Logger LOG = LogManager.getLogger(ApplyServiceImpl.class);

    private static final String ATTR_USER = "user";
    private static final String APPLY_TO="apply_to";
    private static final String PARAMETER_USER_ID="user_id";
    private static final String PARAMETER_FACULTY_ID="faculty_id";

    private static final ApplyServiceImpl instance = new ApplyServiceImpl();

    /**
     *
     * @return ApplyServiceImpl
     * the instance of this class
     */
    public static ApplyServiceImpl getInstance(){
        return instance;
    }

    /**
     * The main server task is to apply documents to faculty.
     * Server work with database.
     * @param request
     * from Command, the request is validated
     * @throws ServiceException
     */
    @Override
    public void doService(HttpServletRequest request) throws ServiceException {
        try {
            Integer facultyId = Integer.parseInt(request.getParameter(APPLY_TO));
            User user = (User)request.getSession(false).getAttribute(ATTR_USER);
            Integer userId = user.getId();

            RegisterDAO registerDAO = SQLRegisterDAO.getInstance();

            Map<String, Integer> mapId = new HashMap<>();
            mapId.put(PARAMETER_USER_ID, userId);
            mapId.put(PARAMETER_FACULTY_ID, facultyId);

            registerDAO.createApplication(mapId);

            user.setAvailable(false);

        } catch (DAOException | NumberFormatException  e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }  catch (Exception e) {
            LOG.error(e.getMessage());
            throw new ServiceException(e);
        }

    }
}
