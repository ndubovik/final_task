package by.epam.training.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 */
public interface Service {
	void doService(HttpServletRequest request) throws ServiceException;
}
