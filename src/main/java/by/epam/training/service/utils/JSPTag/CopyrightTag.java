package by.epam.training.service.utils.JSPTag;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * @author Nastya Dubovik
 * Costum tag copyright
 */
public class CopyrightTag extends TagSupport {

    private static final String FOOTER = "Copyright by Nastya Dubovik 2015 ";
    private static final String FOOTER_TAG_START = "<footer>";
    private static final String FOOTER_TAG_END = "</footer>";

    @Override
    public int doStartTag() throws JspTagException {
        try {
            JspWriter out = pageContext.getOut();
            out.write(FOOTER_TAG_START);
            out.write(FOOTER);

        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_BODY_INCLUDE;
    }


    @Override
    public int doEndTag() throws JspTagException {
        try {
            pageContext.getOut().write(FOOTER_TAG_END);
        } catch (IOException e) {
            throw new JspTagException(e.getMessage());
        }
        return EVAL_PAGE;
    }
}
