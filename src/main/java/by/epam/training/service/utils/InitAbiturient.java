package by.epam.training.service.utils;

import by.epam.training.domain.Abiturient;
import by.epam.training.domain.Subject;
import by.epam.training.domain.SubjectCertificate;
import by.epam.training.domain.User;
import by.epam.training.service.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nastya Dubovik
 * Init abiturient
 */
public class InitAbiturient {
    private static final String PARAMETR_FIRST_NAME = "first_name";
    private static final String PARAMETR_LAST_NAME = "last_name";
    private static final String PARAMETR_PATRONYMIC = "patronymic";
    private static final String PARAMETR_DAY_OF_BIRTH = "day_of_birth";
    private static final String PARAMETR_MONTH_OF_BIRTH = "month_of_birth";
    private static final String PARAMETR_YEAR_OF_BIRTH = "year_of_birth";
    private static final String PARAMETR_PASSSPORT_DATA = "passport_data";
    private static final String PARAMETR_ADRESS_CITY= "city";
    private static final String PARAMETR_ADRESS_STREET= "street";
    private static final String PARAMETR_ADRESS_HOUSE= "house";
    private static final String PARAMETR_ADRESS_FLAT= "flat";
    private static final String PARAMETR_SCHOOL_SCORE= "school_score";
    private static final String PARAMETR_TEST1= "test1";
    private static final String PARAMETR_TEST2= "test2";
    private static final String PARAMETR_TEST3= "test3";
    private static final String PARAMETR_SUBJECT1= "subject1";
    private static final String PARAMETR_SUBJECT2= "subject2";
    private static final String PARAMETR_SUBJECT3= "subject3";
    private static final String PARAMETR_USER = "user";

    /**
     * Init abiturient
     * @param request
     * @return Abiturient
     * entity that was created
     * @throws ServiceException
     */
    public static Abiturient init(HttpServletRequest request) throws ServiceException {
        Abiturient abiturient = new Abiturient();
        try {
            String firstName = request.getParameter(PARAMETR_FIRST_NAME);
            String lastName = request.getParameter(PARAMETR_LAST_NAME);
            String patronymic = request.getParameter(PARAMETR_PATRONYMIC);
            String birthDay = request.getParameter(PARAMETR_DAY_OF_BIRTH);
            String birthMonth = request.getParameter(PARAMETR_MONTH_OF_BIRTH);
            String birthYear = request.getParameter(PARAMETR_YEAR_OF_BIRTH);

            String passportData = request.getParameter(PARAMETR_PASSSPORT_DATA);

            String city = request.getParameter(PARAMETR_ADRESS_CITY);
            String street = request.getParameter(PARAMETR_ADRESS_STREET);
            String house = request.getParameter(PARAMETR_ADRESS_HOUSE);
            String flat = request.getParameter(PARAMETR_ADRESS_FLAT);

            String schoolScore = request.getParameter(PARAMETR_SCHOOL_SCORE);

            String test1 = request.getParameter(PARAMETR_TEST1);
            String test2 = request.getParameter(PARAMETR_TEST2);
            String test3 = request.getParameter(PARAMETR_TEST3);
            String subject1 = request.getParameter(PARAMETR_SUBJECT1);
            String subject2 = request.getParameter(PARAMETR_SUBJECT2);
            String subject3 = request.getParameter(PARAMETR_SUBJECT3);

            int id = ((User)request.getSession(false).getAttribute(PARAMETR_USER)).getId();
            abiturient.setId(id);
            abiturient.setFirstName(firstName);
            abiturient.setLastName(lastName);
            abiturient.setPatronymic(patronymic);
            abiturient.setBirthDay(birthDay);
            abiturient.setBirthMonth(birthMonth);
            abiturient.setBirthYear(birthYear);
            abiturient.setPassportData(passportData);
            abiturient.setCity(city);
            abiturient.setStreet(street);
            abiturient.setHouse(house);
            abiturient.setFlat(flat);
            abiturient.setSchoolScore(Double.parseDouble(schoolScore));
            List<SubjectCertificate> tests = new LinkedList<>();
            tests.add(new SubjectCertificate(id, Integer.parseInt(test1), Subject.valueOf(subject1.toUpperCase())));
            tests.add(new SubjectCertificate(id, Integer.parseInt(test2), Subject.valueOf(subject2.toUpperCase())));
            tests.add(new SubjectCertificate(id, Integer.parseInt(test3), Subject.valueOf(subject3.toUpperCase())));
            abiturient.setTests(tests);


        } catch(NumberFormatException e){
            throw new ServiceException(e);
        }
        return abiturient;

    }
}
