package by.epam.training.service.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Nastya Dubovik
 * Create hashCode in SHA-1
 */
public class PasswordHashCode {

    private static final String COD="SHA-1";

    /**
     *
     * @param password
     * string that will be hashed
     * @return String
     * hashCode in SHA-1
     * @throws NoSuchAlgorithmException
     */
    public static String getHash( String password) throws NoSuchAlgorithmException {
        MessageDigest sha = MessageDigest.getInstance(COD);
        StringBuffer  hexString = new StringBuffer();
        sha.reset();
        sha.update(password.getBytes());
        byte[] array = sha.digest();

        for (int i = 0; i < array.length; i++) {
            hexString.append(Integer.toHexString(0xFF & array[i]));
        }

        return hexString.toString();
    }
}
