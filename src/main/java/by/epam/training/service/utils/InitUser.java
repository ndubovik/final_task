package by.epam.training.service.utils;

import by.epam.training.domain.User;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Nastya Dubovik
 * Init user
 */
public class InitUser {

    private static final String PARAMETR_LOGIN="login";
    private static final String PARAMETR_PASSWORD="password";

    /**
     * Init user
     * @param request
     * @return User
     */
    public static User init(HttpServletRequest request){
        String login = request.getParameter(PARAMETR_LOGIN);
        String password = request.getParameter(PARAMETR_PASSWORD);
        return new User(login, password);
    }
}
