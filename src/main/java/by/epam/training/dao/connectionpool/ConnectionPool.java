package by.epam.training.dao.connectionpool;

import java.sql.Connection;

/**
 * @author Nastya Dubovik
 */
public interface ConnectionPool {
    Connection takeConnection() throws ConnectionPoolException;
    void returnConnection(Connection connection) throws ConnectionPoolException;
}
