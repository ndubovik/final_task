package by.epam.training.dao;

import by.epam.training.domain.Abiturient;

/**
 * @author Nastya Dubovik
 */
public interface AbiturientDAO extends GenericDAO<Abiturient, Integer> {
}
