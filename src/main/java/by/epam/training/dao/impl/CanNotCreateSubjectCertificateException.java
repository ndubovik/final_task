package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception when it is not possible to create user
 */
public class CanNotCreateSubjectCertificateException extends DAOException {
    public CanNotCreateSubjectCertificateException(){}

    /**
     *
     * @param message
     * @param exception
     */
    public CanNotCreateSubjectCertificateException(String message, Throwable exception) {
        super(message, exception);
    }

    /**
     *
     * @param message
     */
    public CanNotCreateSubjectCertificateException(String message) {
        super(message);
    }

    /**
     *
     * @param exception
     */
    public CanNotCreateSubjectCertificateException(Throwable exception) {
        super(exception);
    }
}
