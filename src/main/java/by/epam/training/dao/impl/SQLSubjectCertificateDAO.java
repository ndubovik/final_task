package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOUnsupportedOperationException;
import by.epam.training.dao.SubjectCertificateDAO;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import by.epam.training.domain.Subject;
import by.epam.training.domain.SubjectCertificate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Nastya Dubovik
 */
public class SQLSubjectCertificateDAO implements SubjectCertificateDAO {

    private static final Logger LOG = LogManager.getLogger(SQLSubjectCertificateDAO.class);

    private static final SQLSubjectCertificateDAO instance = new SQLSubjectCertificateDAO();

    private static final String CREATE_SQL = "INSERT INTO SUBJECT_CERTIFICATE (SCORE, ABITURIENT_ID, SUBJECT_ID) VALUES (?, ?, ?)";
    private static final String FIND_SQL = "SELECT * FROM SUBJECT_CERTIFICATE WHERE ABITURIENT_ID=? ";
    private static final String UPDATE_SQL = "UPDATE SUBJECT_CERTIFICATE SET SCORE=? , SUBJECT_ID=? WHERE ID=? ";
    private static final String FIND_ID_SQL = "SELECT ID FROM SUBJECT_CERTIFICATE WHERE ABITURIENT_ID=? ";

    private static final String PARAMETR_ID = "id";
    private static final String PARAMETR_SCORE = "score";
    private static final String PARAMETR_SUBJECT_ID = "subject_id";

    /**
     *
     * @return SQLSubjectCertificateDAO
     * the instance of this class
     */
    public static SQLSubjectCertificateDAO getInstance(){
        return instance;
    }

    /**
     * Create row in table Subject_Certificate.
     * @param subjectCertificate
     * entity that should be created
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     * @see by.epam.training.domain.SubjectCertificate
     * @see by.epam.training.dao.impl.CanNotCreateSubjectCertificateException
     */
    @Override
    public boolean create( SubjectCertificate subjectCertificate) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(CREATE_SQL);

            statement.setInt(1, subjectCertificate.getScore());
            statement.setInt(2, subjectCertificate.getAbiturientId() );
            statement.setInt(3, subjectCertificate.getSubject().getCode());
            countRows = statement.executeUpdate();

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new CanNotCreateSubjectCertificateException(e);
        } finally{
            //return connection into connection pool
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        if( countRows > 0 ){
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param test
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update( SubjectCertificate test) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param key
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete( Integer key) throws DAOException {
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param arrayId
     * @return
     * @throws DAOException
     */
    @Override
    public SubjectCertificate find(Integer arrayId) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param test
     * @return
     * @throws DAOException
     */
    @Override
    public Integer findId( SubjectCertificate test) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Update all certificates
     * @param tests
     * tests that should be updated
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     * @see by.epam.training.domain.SubjectCertificate
     */
    @Override
    public boolean updateAllCertificates( List<SubjectCertificate> tests) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        boolean status = true;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(UPDATE_SQL);

            List<Integer> arrayId = findSetId(tests);

            for(int i=0; i<tests.size(); i++){
                SubjectCertificate sc = tests.get(i);
                statement.setInt(1, sc.getScore());
                statement.setInt(2, sc.getSubject().getCode());
                statement.setInt(3, arrayId.get(i));
                countRows = statement.executeUpdate();

                if(countRows == 0){
                    status = false;
                }

            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        if( status ){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Find subject certificate id
     * @param tests
     * tests by which subject certificate id will be found
     * @return List<Integer>
     * list of subject certificate id if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     * @see by.epam.training.domain.SubjectCertificate
     */
    @Override
    public List<Integer> findSetId( List<SubjectCertificate> tests) throws DAOException{
        Connection connection = null;
        List<Integer> id = new LinkedList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_ID_SQL);

            int abiturientId = tests.get(0).getAbiturientId();
            statement.setInt(1, abiturientId);

            resultSet = statement.executeQuery();

            while( resultSet.next() ){
                id.add(resultSet.getInt(PARAMETR_ID));
            }

            if(id.size() == 0){
                id = Collections.emptyList();
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }
        return id;
    }

    /**
     * Find subject certificate by abiturient id
     * @param abiturientId
     * id og abiturient
     * @return List<SubjectCertificate>
     * list of subject certificates if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     * @see by.epam.training.domain.SubjectCertificate
     */
    public List<SubjectCertificate> findByAbiturient(Integer abiturientId) throws DAOException{
        Connection connection = null;
        List<SubjectCertificate> tests = new LinkedList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_SQL);

            statement.setInt(1, abiturientId);

            resultSet = statement.executeQuery();

            while( resultSet.next() ){
                int id = resultSet.getInt(PARAMETR_ID);
                int score = resultSet.getInt(PARAMETR_SCORE);
                int subjectCode = resultSet.getInt(PARAMETR_SUBJECT_ID);
                tests.add(new SubjectCertificate(id, score, abiturientId, Subject.getSubject(subjectCode)));
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }
        return tests;
    }

}
