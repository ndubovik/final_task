package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;

/**
 * @author Nastya Dubovik
 * Describe an exception when it is not possible to create an abiturient
 */
public class CanNotCreateAbiturientException extends DAOException {
    public CanNotCreateAbiturientException(){}

    /**
     *
     * @param message
     * @param exception
     */
    public CanNotCreateAbiturientException(String message, Throwable exception) {
        super(message, exception);
    }

    /**
     *
     * @param message
     */
    public CanNotCreateAbiturientException(String message) {
        super(message);
    }

    /**
     *
     * @param exception
     */
    public CanNotCreateAbiturientException(Throwable exception) {
        super(exception);
    }
}
