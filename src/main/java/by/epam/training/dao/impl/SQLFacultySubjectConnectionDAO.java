package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOUnsupportedOperationException;
import by.epam.training.dao.FacultySubjectConnectionDAO;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Nastya Dubovik
 */
public class SQLFacultySubjectConnectionDAO implements FacultySubjectConnectionDAO{

    private static final Logger LOG = LogManager.getLogger(SQLFacultySubjectConnectionDAO.class);

    private static final String SELECT_FACULTY_ID_SQL = "SELECT FACULTY_ID FROM FAC_SUB_CONNECTION WHERE SUBJECT_ID = ? ";

    private static final String PARAMETER_FACULTY_ID = "faculty_id";

    private static final SQLFacultySubjectConnectionDAO instance = new SQLFacultySubjectConnectionDAO();

    /**
     *
     * @return SQLFacultySubjectConnectionDAO
     * the instance of this class
     */
    public static SQLFacultySubjectConnectionDAO getInstance(){
        return instance;
    }

    /**
     *
     * @param entity
     * @return
     * @throws DAOException
     */
    @Override
    public boolean create( List<Integer> entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param entity
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update(List<Integer> entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param key
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer key) throws DAOException {
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Find faculties by subject id.
     * It is necessary to pass the test of this subject(id of which
     * is came as parameter) to enter the faculty
     * @param subjectId
     * id of subject
     * @return List<Integer>
     * list of faculties id if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public List<Integer> find(Integer subjectId) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Integer> facultiesId = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(SELECT_FACULTY_ID_SQL);

            statement.setInt(1, subjectId);
            resultSet = statement.executeQuery();

            facultiesId = new LinkedList<>();

            while( resultSet.next() ) {
                Integer universityId = resultSet.getInt(PARAMETER_FACULTY_ID);
                facultiesId.add(universityId);
            }

            if(facultiesId == null ){
                facultiesId = Collections.emptyList();
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            //return connection into connection pool
            try {
                //����� ���� ��������� �� autoCommit
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        return facultiesId;
    }

    /**
     *
     * @param test
     * @return
     * @throws DAOException
     */
    @Override
    public Integer findId(List<Integer> test) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

}
