package by.epam.training.dao.impl;

import by.epam.training.dao.AbiturientDAO;
import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOUnsupportedOperationException;
import by.epam.training.dao.SubjectCertificateDAO;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import by.epam.training.domain.Abiturient;
import by.epam.training.domain.SubjectCertificate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Nastya Dubovik
 * Describe DAO that work with sql database with abiturient table
 * @see by.epam.training.dao.AbiturientDAO
 */
public class SQLAbiturientDAO implements AbiturientDAO {

    private static final Logger LOG = LogManager.getLogger(SQLAbiturientDAO.class);

    private static final SQLAbiturientDAO instance = new SQLAbiturientDAO();

    private static final String PARAMETR_FIRST_NAME = "first_name";
    private static final String PARAMETR_LAST_NAME = "last_name";
    private static final String PARAMETR_PATRONYMIC = "patronymic";
    private static final String PARAMETR_DATE_OF_BIRTH = "date_of_birth";
    private static final String PARAMETR_PASSSPORT_DATA = "passport_data";
    private static final String PARAMETR_ADRESS= "adress";
    private static final String PARAMETR_SCHOOL_SCORE= "score";
    private static final String PARAMETR_USER_ID= "iser_ID";

    private static final String CHECK_ABITURIENT_SQL= "SELECT * FROM ABITURIENT WHERE FIRST_NAME=? AND LAST_NAME=? AND PATRONYMIC=? AND DATE_OF_BIRTH=? AND PASSPORT_DATA=? AND ADRESS=? AND SCORE=? ;";
    private static final String CREATE_ABITURIENT_SQL= "INSERT INTO ABITURIENT (USER_ID, FIRST_NAME, LAST_NAME, PATRONYMIC, DATE_OF_BIRTH, PASSPORT_DATA, ADRESS, SCORE) VALUES(?, ?, ?, ?, ?, ?, ?, ?) ;";
    private static final String FIND_ABITURIENT_SQL = "SELECT * FROM ABITURIENT WHERE USER_ID=?";
    private static final String UPDATE_ABITURIENT_SQL = "UPDATE ABITURIENT SET FIRST_NAME=? , LAST_NAME=? , PATRONYMIC=? , DATE_OF_BIRTH=? , PASSPORT_DATA=? , ADRESS=? , SCORE=? WHERE USER_ID=? ; ";

    /**
     *
     * @return SQLAbiturientDAO
     * the instance of this class
     */
    public static SQLAbiturientDAO getInstance(){
        return instance;
    }

    /**
     * Create a row in table Abiturient
     * @param abiturient
     * entity that should be created
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.domain.Abiturient
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public boolean create(Abiturient abiturient) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        boolean status=true;
        try {
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(CREATE_ABITURIENT_SQL);

            connection.setAutoCommit(false);

            statement.setInt(1, abiturient.getId());
            statement.setString(2, abiturient.getFirstName());
            statement.setString(3, abiturient.getLastName() );
            statement.setString(4, abiturient.getPatronymic());
            statement.setString(5, abiturient.formDateOfBirth());
            statement.setString(6, abiturient.getPassportData() );
            statement.setString(7, abiturient.formAdress());
            statement.setDouble(8, abiturient.getSchoolScore());
            countRows = statement.executeUpdate();

            if( countRows > 0){
                SubjectCertificateDAO subjectCertificateDAO = SQLSubjectCertificateDAO.getInstance();
                for(SubjectCertificate sc : abiturient.getTests()){
                    if(status){
                        status = subjectCertificateDAO.create(sc);
                    }
                }
            } else {
                status = false;
            }

            if (statement != null) {
                statement.close();
            }

            if(!status){
                connection.rollback();
                SQLUserDAO.getInstance().delete(abiturient.getId());
            }
            connection.setAutoCommit(true);

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            SQLUserDAO.getInstance().delete(abiturient.getId());
            throw new CanNotCreateAbiturientException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }
        if( status ){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a row in table Abiturient
     * @param abiturient
     * entity that should be updated
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.domain.Abiturient
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public boolean update(Abiturient abiturient) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        boolean status = true;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(UPDATE_ABITURIENT_SQL);

            connection.setAutoCommit(false);

            statement.setString(1, abiturient.getFirstName());
            statement.setString(2, abiturient.getLastName());
            statement.setString(3, abiturient.getPatronymic());
            statement.setString(4, abiturient.formDateOfBirth());
            statement.setString(5, abiturient.getPassportData());
            statement.setString(6, abiturient.formAdress());
            statement.setDouble(7, abiturient.getSchoolScore());
            statement.setInt(8, abiturient.getId());

            countRows = statement.executeUpdate();

            if(countRows > 0){
                SubjectCertificateDAO subjectCertificateDAO = SQLSubjectCertificateDAO.getInstance();
                status  = subjectCertificateDAO.updateAllCertificates(abiturient.getTests());
            } else {
                status = false;
            }

            if (statement != null) {
                statement.close();
            }

            if(!status){
                connection.rollback();
            }
            connection.setAutoCommit(true);

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        if( status ){
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param key
     * @return
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer key) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Find abiturient by id
     * @param id
     * id of abiturient that should be find
     * @return Abiturient
     * object if everything go correct, else null
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.domain.Abiturient
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     * @see by.epam.training.dao.impl.SQLSubjectCertificateDAO
     */
    @Override
    public Abiturient find(Integer id) throws DAOException{
        Connection connection = null;
        Abiturient abiturient = new Abiturient();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_ABITURIENT_SQL);
            statement.setInt(1, id);

            resultSet = statement.executeQuery();

            if( resultSet.next() ){
                String first_name = resultSet.getString(PARAMETR_FIRST_NAME);
                String last_name = resultSet.getString(PARAMETR_LAST_NAME);
                String patronymic = resultSet.getString(PARAMETR_PATRONYMIC);
                String dateOfBirth = resultSet.getDate(PARAMETR_DATE_OF_BIRTH).toString();
                String passport_data = resultSet.getString(PARAMETR_PASSSPORT_DATA);
                String adress = resultSet.getString(PARAMETR_ADRESS);
                Double school_score = resultSet.getDouble(PARAMETR_SCHOOL_SCORE);

                abiturient.setId(id);
                abiturient.setFirstName(first_name);
                abiturient.setLastName(last_name);
                abiturient.setPatronymic(patronymic);
                String[] dateArray = dateOfBirth.split("-");
                abiturient.setBirthYear(dateArray[0]);
                abiturient.setBirthMonth(dateArray[1]);
                abiturient.setBirthDay(dateArray[2]);
                abiturient.setPassportData(passport_data);
                String[] adress_array = adress.split("\\s");
                abiturient.setCity(adress_array[0]);
                abiturient.setStreet(adress_array[1]);
                abiturient.setHouse(adress_array[2]);
                abiturient.setFlat(adress_array[3]);
                abiturient.setSchoolScore(school_score);

            }

            /**
             * Find abiturient tests
             */
            SubjectCertificateDAO subjectCertificateDAO = SQLSubjectCertificateDAO.getInstance();
            abiturient.setTests(subjectCertificateDAO.findByAbiturient(id));

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }
        return abiturient;
    }

    /**
     * Find id by abiturient
     * @param abiturient
     * abiturient whose id should be find
     * @return Integer
     * object if everything go correct, else null
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.domain.Abiturient
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public Integer findId(Abiturient abiturient) throws DAOException{
        Connection connection = null;
        Integer id = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(CHECK_ABITURIENT_SQL);

            statement.setString(1, abiturient.getFirstName());
            statement.setString(2, abiturient.getLastName() );
            statement.setString(3, abiturient.getPatronymic() );

            statement.setString(4, abiturient.formDateOfBirth());
            statement.setString(5, abiturient.getPassportData() );
            statement.setString(6, abiturient.formAdress() );
            statement.setDouble(7, abiturient.getSchoolScore());

            resultSet = statement.executeQuery();

            if( resultSet.next() ){
                id = resultSet.getInt(PARAMETR_USER_ID);
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }
        return id;
    }


}
