package by.epam.training.dao.impl;

import by.epam.training.dao.*;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import by.epam.training.domain.Faculty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Nastya Dubovik
 */
public class SQLFacultyDAO implements FacultyDAO {

    private static final Logger LOG = LogManager.getLogger(SQLFacultyDAO.class);

    private static final SQLFacultyDAO instance = new SQLFacultyDAO();

    private static final String FIND_SQL = "SELECT * FROM FACULTY WHERE ID = ? ";

    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_PREVIOUS_SCORE = "previous_passing_score";
    private static final String PARAMETER_PLAN = "plan";

    /**
     *
     * @return SQLFacultyDAO
     * the instance of this class
     */
    public static SQLFacultyDAO getInstance(){
        return instance;
    }

    /**
     *
     * @param entity
     * @return boolean
     * @throws DAOException
     */
    @Override
    public boolean create( Faculty entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param entity
     * @return boolean
     * @throws DAOException
     */
    @Override
    public boolean update( Faculty entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param key
     * @return boolean
     * @throws DAOException
     */
    @Override
    public boolean delete(Integer key) throws DAOException {
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Find faculty by id
     * @param facultyId
     * id of faculty that should be find
     * @return Abiturient
     * object if everything go correct, else null
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.domain.Faculty
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public Faculty find(Integer facultyId) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Faculty faculty = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_SQL);

            statement.setInt(1, facultyId);
            resultSet = statement.executeQuery();


            if( resultSet.next() ) {
                String name = resultSet.getString(PARAMETER_NAME);
                int previous_passing_score = resultSet.getInt(PARAMETER_PREVIOUS_SCORE);
                int plan = resultSet.getInt(PARAMETER_PLAN);
                faculty = new Faculty(facultyId, name, previous_passing_score, plan);
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        return faculty;
    }

    /**
     *
     * @param faculty
     * @return
     * @throws DAOException
     */
    @Override
    public Integer findId( Faculty faculty ) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

}
