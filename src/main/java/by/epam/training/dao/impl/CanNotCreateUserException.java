package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;

/**
 * Created by ��������� on 12/9/2015.
 * Describe an exception when it is not possible
 */
public class CanNotCreateUserException extends DAOException {
    public CanNotCreateUserException(){}

    /**
     *
     * @param message
     * @param exception
     */
    public CanNotCreateUserException(String message, Throwable exception) {
        super(message, exception);
    }

    /**
     *
     * @param message
     */
    public CanNotCreateUserException(String message) {
        super(message);
    }

    /**
     *
     * @param exception
     */
    public CanNotCreateUserException(Throwable exception) {
        super(exception);
    }
}
