package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOUnsupportedOperationException;
import by.epam.training.dao.RegisterDAO;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author Nastya Dubovik
 */
public class SQLRegisterDAO implements RegisterDAO {

    private static final Logger LOG = LogManager.getLogger(SQLRegisterDAO.class);

    private static final SQLRegisterDAO instance = new SQLRegisterDAO();

    private static final String FIND_ABITURIENTS_BY_FACULTY_SQL = "SELECT * FROM REGISTER WHERE FACULTY_ID = ? ";
    private static final String FIND_FACULTIES_BY_ABITURIENT_SQL = "SELECT * FROM REGISTER WHERE ABITURIENT_ID = ? ";
    private static final String CREATE_SQL = "INSERT INTO REGISTER (abiturient_id, faculty_id) VALUES(?, ?)";
    private static final String FIND_ALL_ABITURIENTS_SQL = "SELECT ABITURIENT_ID FROM REGISTER ";
    private static final String DELETE_SQL = "DELETE FROM REGISTER WHERE ABITURIENT_ID=? AND FACULTY_ID=? ";

    private static final String PARAMETER_FACULTY_ID = "faculty_id";
    private static final String PARAMETER_ABITURIENT_ID = "abiturient_id";
    private static final String PARAMETER_USER_ID="user_id";

    /**
     *
     * @return SQLRegisterDAO
     * the instance of this class
     */
    public static SQLRegisterDAO getInstance(){
        return instance;
    }

    /**
     *
     * @param key
     * @return
     * @throws DAOException
     */
    @Override
    public boolean create( Integer key ) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param entity
     * @return
     * @throws DAOException
     */
    @Override
    public boolean update( Integer entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Delete row by user id and faculty id.
     * @param mapId
     * keySet is UserId and FacultyId
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public boolean delete(Map<String, Integer> mapId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(DELETE_SQL);

            statement.setInt(1, mapId.get(PARAMETER_USER_ID));
            statement.setInt(2, mapId.get(PARAMETER_FACULTY_ID));

            countRows = statement.executeUpdate();

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        if(countRows>0 ){
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param key
     * @return
     * @throws DAOException
     */
    @Override
    public Integer find(Map<String, Integer> key) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     *
     * @param entity
     * @return
     * @throws DAOException
     */
    @Override
    public Map<String, Integer> findId(Integer entity) throws DAOException{
        throw new DAOUnsupportedOperationException();
    }

    /**
     * Create application.
     * @param arrayId
     * keySet is UserId and FacultyId
     * @return boolean
     * true if everything go correct, else false
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public boolean createApplication(  Map<String, Integer> arrayId) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        int countRows;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(CREATE_SQL);

            statement.setInt(1, arrayId.get(PARAMETER_USER_ID));
            statement.setInt(2, arrayId.get(PARAMETER_FACULTY_ID));

            countRows = statement.executeUpdate();

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            //return connection into connection pool
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        if(countRows > 0){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Find abiturients that applied documents to concrete faculty
     * @param facultyId
     * id of faculty
     * @return List<Integer>
     * list of abiturients id if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public List<Integer> findAbiturientsByFaculty(Integer facultyId) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Integer> abiturientsId = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_ABITURIENTS_BY_FACULTY_SQL);

            statement.setInt(1, facultyId);
            resultSet = statement.executeQuery();

            abiturientsId = new LinkedList<>();

            while( resultSet.next() ) {
                Integer abiturientId = resultSet.getInt(PARAMETER_ABITURIENT_ID);
                abiturientsId.add(abiturientId);
            }

            if(abiturientsId == null){
                abiturientsId = Collections.emptyList();
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        return abiturientsId;
    }

    /**
     * Find faculties that abiturient had chosen
     * @param abiturientId
     * id of abiturient
     * @return List<Integer>
     * list of faculties id if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public List<Integer> findFacultiesByAbiturient( Integer abiturientId ) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Integer> facultiesId = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_FACULTIES_BY_ABITURIENT_SQL);

            statement.setInt(1, abiturientId);
            resultSet = statement.executeQuery();

            facultiesId = new LinkedList<>();

            while( resultSet.next() ) {
                Integer facultyId = resultSet.getInt(PARAMETER_FACULTY_ID);
                facultiesId.add(facultyId);
            }

            if(facultiesId == null){
                facultiesId = Collections.emptyList();
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            //return connection into connection pool
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        return facultiesId;
    }

    /**
     * Find all abiturients that applied documents to any faculty.
     * In other words - all abiturients in table Register
     * @param
     * @return List<Integer>
     * list of abiturients id if everything go correct, else emptyList
     * @throws DAOException
     * @see by.epam.training.dao.DAOException
     * @see by.epam.training.dao.connectionpool.ConnectionPoolException
     * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
     */
    @Override
    public List<Integer> findAllAbiturients( ) throws DAOException{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Integer> abiturientId = null;
        try{
            connection = ConnectionPoolImpl.getInstance().takeConnection();

            statement = connection.prepareStatement(FIND_ALL_ABITURIENTS_SQL);
            resultSet = statement.executeQuery();

            abiturientId = new LinkedList<>();

            while( resultSet.next() ) {
                Integer facultyId = resultSet.getInt(PARAMETER_ABITURIENT_ID);
                abiturientId.add(facultyId);
            }

            if(abiturientId == null){
                abiturientId = Collections.emptyList();
            }

            if (statement != null) {
                statement.close();
            }

        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e.getMessage());
            throw new DAOException(e);
        } finally{
            //return connection into connection pool
            try {
                ConnectionPoolImpl.getInstance().returnConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e.getMessage());
                throw new DAOException(e);
            }
        }

        return abiturientId;
    }

}
