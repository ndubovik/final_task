package by.epam.training.dao.impl;

import by.epam.training.dao.DAOException;
import by.epam.training.dao.DAOUnsupportedOperationException;
import by.epam.training.dao.UserDAO;
import by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl;
import by.epam.training.dao.connectionpool.ConnectionPoolException;
import by.epam.training.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * @author Nastya Dubovik
 */
public class SQLUserDAO implements UserDAO {

	private static final Logger LOG = LogManager.getLogger(SQLUserDAO.class);

	private static final SQLUserDAO instance = new SQLUserDAO();

	private static final String SELECT_SQL= "SELECT * FROM USER WHERE LOGIN=? ";
	private static final String CREATE_SQL= "INSERT INTO USER (LOGIN, PASSWORD) VALUES(?, ?) ";
	private static final String DELETE_SQL= "DELETE FROM USER WHERE USER_ID = ? ";

	private static final String PARAMETR_USER_ID = "user_ID";

	/**
	 *
	 * @return SQLRegisterDAO
	 * the instance of this class
	 */
	public static SQLUserDAO getInstance(){
		return instance;
	}

	/**
	 * Create row in table User
	 * @param user
	 * entity that should be created
	 * @return boolean
	 * true if everything go correct, else false
	 * @throws DAOException
	 * @see by.epam.training.dao.DAOException
	 * @see by.epam.training.dao.connectionpool.ConnectionPoolException
	 * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
	 */
	@Override
	public boolean create(User user) throws DAOException{
		Connection connection = null;
		PreparedStatement statement = null;
		int countRows;
		try{
			connection = ConnectionPoolImpl.getInstance().takeConnection();

			statement = connection.prepareStatement(CREATE_SQL);

			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			countRows = statement.executeUpdate();

			if (statement != null) {
				statement.close();
			}

		} catch (ConnectionPoolException | SQLException e) {
			LOG.error(e.getMessage());
			throw new CanNotCreateUserException(e);
		} finally{
			try {
				ConnectionPoolImpl.getInstance().returnConnection(connection);
			} catch (ConnectionPoolException e) {
				LOG.error(e.getMessage());
				throw new DAOException(e);
			}
		}

		if( countRows > 0){
			return true;
		} else {
			return false;
		}

	}

	/**
	 *
	 * @param user
	 * @return
	 * @throws DAOException
	 */
	@Override
	public boolean update(User user) throws DAOException{
		throw new DAOUnsupportedOperationException();
	}

	/**
	 * Delete row from table User
	 * @param key
	 * id of user that should be deleted
	 * @return boolean
	 * true if everything go correct, else false
	 * @throws DAOException
	 * @see by.epam.training.dao.DAOException
	 * @see by.epam.training.dao.connectionpool.ConnectionPoolException
	 * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
	 */
	@Override
	public boolean delete(Integer key) throws DAOException{
		Connection connection = null;
		PreparedStatement statement = null;
		int countRows;
		try{
			connection = ConnectionPoolImpl.getInstance().takeConnection();

			statement = connection.prepareStatement(DELETE_SQL);

			statement.setInt(1, key);
			countRows = statement.executeUpdate();

			if (statement != null) {
				statement.close();
			}

		} catch (ConnectionPoolException | SQLException e) {
			LOG.error(e.getMessage());
			throw new CanNotCreateUserException(e);
		} finally{
			try {
				ConnectionPoolImpl.getInstance().returnConnection(connection);
			} catch (ConnectionPoolException e) {
				LOG.error(e.getMessage());
				throw new DAOException(e);
			}
		}

		if( countRows > 0){
			return true;
		} else {
			return false;
		}
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws DAOException
	 */
	@Override
	public User find(Integer id) throws DAOException {
		throw new DAOUnsupportedOperationException();
	}

	/**
	 * Find id of user
	 * @param user
	 * user whise id should be find
	 * @return Integer
	 * id of user if everything go correct, else null
	 * @throws DAOException
	 * @see by.epam.training.dao.DAOException
	 * @see by.epam.training.dao.connectionpool.ConnectionPoolException
	 * @see by.epam.training.dao.connectionpool.impl.ConnectionPoolImpl
	 */
	@Override
	public Integer findId(User user) throws DAOException{
		Connection connection = null;
		Integer id = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try{
			connection = ConnectionPoolImpl.getInstance().takeConnection();


			statement = connection.prepareStatement(SELECT_SQL);

			statement.setString(1, user.getLogin());

			resultSet = statement.executeQuery();

			if( resultSet.next() ){
				id = resultSet.getInt(PARAMETR_USER_ID);
			}

			if (statement != null) {
				statement.close();
			}

		} catch (ConnectionPoolException | SQLException e) {
			LOG.error(e.getMessage());
			throw new DAOException(e);
		} finally{
			try {
				ConnectionPoolImpl.getInstance().returnConnection(connection);
			} catch (ConnectionPoolException e) {
				LOG.error(e.getMessage());
				throw new DAOException(e);
			}
		}
		return id;
	}

}
