package by.epam.training.dao;

import by.epam.training.domain.SubjectCertificate;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface SubjectCertificateDAO extends GenericDAO<SubjectCertificate, Integer> {
    List<SubjectCertificate> findByAbiturient(Integer abiturientId) throws DAOException;
    boolean updateAllCertificates( List<SubjectCertificate> tests) throws DAOException;
    List<Integer> findSetId( List<SubjectCertificate> tests) throws DAOException;
}
