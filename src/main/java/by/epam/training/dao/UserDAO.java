package by.epam.training.dao;


import by.epam.training.domain.User;

/**
 * @author Nastya Dubovik
 */
public interface UserDAO extends GenericDAO<User, Integer>{
}
