package by.epam.training.dao;

/**
 * @author Nastya Dubovik
 */
public interface GenericDAO<T,K> {
    boolean create(T entity) throws DAOException;
    boolean update(T entity) throws DAOException;
    boolean delete(K key) throws DAOException;
    T find(K key) throws DAOException;
    K findId(T entity) throws DAOException;
}
