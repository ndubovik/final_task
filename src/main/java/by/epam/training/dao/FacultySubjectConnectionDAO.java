package by.epam.training.dao;

import java.util.List;

/**
 * @author Nastya Dubovik
 */
public interface FacultySubjectConnectionDAO extends GenericDAO<List<Integer>, Integer> {
    //� �������� key K - subjectId
    //� �������� entity T - List<Int>
}
