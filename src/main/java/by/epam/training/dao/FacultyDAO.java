package by.epam.training.dao;

import by.epam.training.domain.Faculty;

/**
 * @author Nastya Dubovik
 */
public interface FacultyDAO extends GenericDAO<Faculty, Integer> {
}
