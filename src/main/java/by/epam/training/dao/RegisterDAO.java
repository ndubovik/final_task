package by.epam.training.dao;

import java.util.List;
import java.util.Map;

/**
 * @author Nastya Dubovik
 */
public interface RegisterDAO extends GenericDAO<Integer, Map<String, Integer>> {
    List<Integer> findAbiturientsByFaculty (Integer facultyId) throws DAOException;
    List<Integer> findFacultiesByAbiturient(Integer abiturientId) throws DAOException;
    List<Integer> findAllAbiturients() throws DAOException;
    boolean createApplication( Map<String, Integer> arrayId) throws DAOException;
}
