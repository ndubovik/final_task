package by.epam.training.dao;

/**
 * @author Nastya Dubovik
 * Describes exception like unsupported operation
 */
public class DAOUnsupportedOperationException extends DAOException {
    public DAOUnsupportedOperationException(){}
    public DAOUnsupportedOperationException(String message, Throwable exception) {
        super(message, exception);
    }
    public DAOUnsupportedOperationException(String message) {
        super(message);
    }
    public DAOUnsupportedOperationException(Throwable exception) {
        super(exception);
    }
}
