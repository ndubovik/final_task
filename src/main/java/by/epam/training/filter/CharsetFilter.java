package by.epam.training.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author Nastya Dubovik
 */
public class CharsetFilter implements Filter {

    private static final Logger LOG = LogManager.getLogger(CharsetFilter.class);

    private static final String INIT_PARAMETER = "characterEncoding";

    private String encoding;
    private ServletContext context;

    @Override
    public void destroy(){
        context = null;
    }

    /**
     * Init encoding
     * @param fConfig
     */
    @Override
    public void init(FilterConfig fConfig){
        encoding = fConfig.getInitParameter(INIT_PARAMETER);
        context = fConfig.getServletContext();
        context.log("CharsetFilter is initialized.");
    }

    /**
     * Set encoding
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {

        request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);
        context.log("Charset was set.");

        chain.doFilter(request, response);
    }

}
