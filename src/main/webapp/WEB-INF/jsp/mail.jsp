<%--
  Created by IntelliJ IDEA.
  User: Настенька
  Date: 11/23/2015
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/taglib.tld" %>
<html>
<head>
  <fmt:setLocale value="${sessionScope.locale}"/>
  <fmt:setBundle basename="localize" var="loc"/>
  <fmt:message bundle="${loc}" key="local.title.mail" var="title"/>
  <fmt:message bundle="${loc}" key="local.title.form" var="form"/>
  <fmt:message bundle="${loc}" key="local.title.main" var="main"/>
  <fmt:message bundle="${loc}" key="local.title.apply" var="apply"/>
  <fmt:message bundle="${loc}" key="local.ru" var="ru"/>
  <fmt:message bundle="${loc}" key="local.en" var="en"/>
  <fmt:message bundle="${loc}" key="local.greeting" var="greeting"/>
  <fmt:message bundle="${loc}" key="local.button.logout" var="logout"/>

  <fmt:message bundle="${loc}" key="local.main.faculties_list" var="faculties_list"/>
  <fmt:message bundle="${loc}" key="local.main.passing_score" var="passing_score"/>
  <fmt:message bundle="${loc}" key="local.main.plan" var="plan"/>
  <fmt:message bundle="${loc}" key="local.main.amount_of_applications" var="amount_of_applications"/>
  <fmt:message bundle="${loc}" key="local.main.phrase" var="phrase"/>

  <fmt:message bundle="${loc}" key="local.mail.pick_up" var="pick_up"/>
  <fmt:message bundle="${loc}" key="local.mail.your_application" var="your_application"/>

  <title>${title}</title>
  <link rel="stylesheet" href="css/main/style.css" type="text/css" />
  <link rel="stylesheet" href="css/main/style2.css" type="text/css" />
</head>
<body>


<header>
  <div>
    <p>${sessionScope.get("user").getLogin()}</p>
    <form action="Controller" method="post">
      <input type="hidden" name="action" value="logout"/>
      <input type="submit" value="${logout}"/>
    </form>
    <nav>
      <ul>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="main"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${main}"/>
          </form>
        </li>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="form"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${form}"/>
          </form>
        </li>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="mail"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${title}"/>
          </form>
        </li>
      </ul>
    </nav>
  </div>
</header>

<div class="content">

  <div class="comment">
    <img src="img/comment.png"/>
    <p>${phrase}</p>
  </div>

  <section>

    <c:forEach var="faculty" items="${requestScope.faculties}">
      <div class ="faculty">
        <p><c:out value="${faculty.getName()}"/></p>
        <p>${passing_score}: <c:out value="${faculty.getPreviousPassingScore()}"/></p>
        <p>${plan}: <c:out value="${faculty.getPlan()}"/></p>
        <p>${amount_of_applications}: <c:out value="${faculty.getAbiturientAmount()}"/></p>
        <form ation="Controller" method="post">
          <input type="hidden" name="action" value="pick_up"/>
          <input type="hidden" name="pick_up_from" value="${faculty.getId()}"/>
          <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
          <input type="submit" value="${pick_up}"/>
        </form>
      </div>
    </c:forEach>

  </section>

</div>

<mytag:copyright/>

</body>
</html>
