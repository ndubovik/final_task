<%--
  Created by IntelliJ IDEA.
  User: Настенька
  Date: 11/8/2015
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <fmt:setLocale value="${sessionScope.locale}"/>
  <fmt:setBundle basename="localize" var="loc"/>
  <fmt:message bundle="${loc}" key="local.title.registration" var="title"/>
  <fmt:message bundle="${loc}" key="local.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.password" var="password"/>
  <fmt:message bundle="${loc}" key="local.register" var="register"/>
  <fmt:message bundle="${loc}" key="local.ru" var="ru"/>
  <fmt:message bundle="${loc}" key="local.en" var="en"/>
  <fmt:message bundle="${loc}" key="local.error.message" var="message"/>
  <fmt:message bundle="${loc}" key="local.button.goback" var="goback"/>
  <fmt:message bundle="${loc}" key="local.enter" var="enter"/>

  <fmt:message bundle="${loc}" key="local.registration.fio" var="fio"/>
  <fmt:message bundle="${loc}" key="local.registration.first_name" var="first_name"/>
  <fmt:message bundle="${loc}" key="local.registration.last_name" var="last_name"/>
  <fmt:message bundle="${loc}" key="local.registration.patronymic" var="patronymic"/>
  <fmt:message bundle="${loc}" key="local.registration.date_of_birth" var="date_of_birth"/>
  <fmt:message bundle="${loc}" key="local.registration.day" var="day"/>
  <fmt:message bundle="${loc}" key="local.registration.month" var="month"/>
  <fmt:message bundle="${loc}" key="local.registration.year" var="year"/>
  <fmt:message bundle="${loc}" key="local.registration.passport_data" var="passport_data"/>
  <fmt:message bundle="${loc}" key="local.registration.adress" var="adress"/>
  <fmt:message bundle="${loc}" key="local.registration.city" var="city"/>
  <fmt:message bundle="${loc}" key="local.registration.street" var="street"/>
  <fmt:message bundle="${loc}" key="local.registration.house" var="house"/>
  <fmt:message bundle="${loc}" key="local.registration.flat" var="flat"/>
  <fmt:message bundle="${loc}" key="local.registration.school_score" var="school_score"/>
  <fmt:message bundle="${loc}" key="local.registration.subject" var="subject"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.russian_lang" var="russian_lang"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.belarusian_lang" var="belarusian_lang"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.math" var="math"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.physics" var="physics"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.chemistry" var="chemistry"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.biology" var="biology"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.german" var="german"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.english" var="english"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.spanish" var="spanish"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.french" var="french"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.history_of_belarus" var="history_of_belarus"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.world_history" var="world_history"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.civics" var="civics"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.geography" var="geography"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.score" var="score"/>

  <meta charset="UTF-8">
  <title>${title}</title>
  <link rel="stylesheet" href="css/registration/style.css" type="text/css" />
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>



<div class="main">
  <header>
    <a href="my-webapp/index.jsp">${goback}</a>
    <h1>${register}</h1>
  </header>


  <form action="Controller" method="post" name="registration" class="clearfix">
    <input type="hidden" name="action" value="register"/>
    <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
    <div>
      <label for="login">${login}:
        <input type="email" name="login" id="login" placeholder="${login}" required="">
      </label>
      <label for="password">${password}:
        <input type="password" name="password" id="password" placeholder="${password}" required="">
      </label>
    </div>
    <div>
      <label for="last_name">${fio}:
        <input type="text" name="last_name" id="last_name" placeholder="${last_name}" required="">
        <input type="text" name="first_name" id="first_name" placeholder="${first_name}" required="">
        <input type="text" name="patronymic" id="middle_name" placeholder="${patronymic}" required="">
      </label>
    </div>
    <div>
      <label for="passport">${passport_data}:</label>
      <input type="text" name="passport_data" id="passport" placeholder="MP1110099" required="">
    </div>
    <div>
      <label>${date_of_birth}:
        <label for="day">${day}</label>
        <select name="day_of_birth" id="day"></select>
        <label for="month">${month}</label>
        <select name="month_of_birth" id="month"> </select>
        <label for="year">${year}</label>
        <select name="year_of_birth" id="year"> </select>
      </label>
    </div>
    <div>
      <label class="adress">${adress}:
        <input type="text" name="city" id="city" placeholder="${city}" required="">
        <input type="text" name="street" id="street" placeholder="${street}" required="">
        <input type="text" name="house" id="house" placeholder="${house}" required="">
        <input type="text" name="flat" id="flat" placeholder="${flat}" required="">
      </label>
    </div>
    <div>
      <label for="certificate">${school_score}</label>
      <input type="text" name="school_score" id="certificate" placeholder="9.7" required="">
    </div>
    <div>
                <span>${enter} ${subject}:
                    <select name="subject1" id="subject">
                      <option value="russian_lang">${russian_lang}</option>
                      <option value="belorusian_lang">${belarusian_lang}</option>
                      <option value="math">${math}</option>
                      <option value="physics">${physics}</option>
                      <option value="chemistry">${chemistry}</option>
                      <option value="biology">${biology}</option>
                      <option value="german">${german}</option>
                      <option value="english">${english}</option>
                      <option value="spanish">${spanish}</option>
                      <option value="french">${french}</option>
                      <option value="history_of_belarus">${history_of_belarus}</option>
                      <option value="world_history">${world_history}</option>
                      <option value="civics">${civics}</option>
                      <option value="geography">${geography}</option>
                    </select> ${score}
                    <input type="text" name="test1" id="subject_mark" required="" placeholder="100">
                </span>
    </div>
    <div>
                <span>${enter} ${subject}:
                    <select name="subject2" id="subject">
                      <option value="russian_lang">${russian_lang}</option>
                      <option value="belorusian_lang">${belarusian_lang}</option>
                      <option value="math">${math}</option>
                      <option value="physics">${physics}</option>
                      <option value="chemistry">${chemistry}</option>
                      <option value="biology">${biology}</option>
                      <option value="german">${german}</option>
                      <option value="english">${english}</option>
                      <option value="spanish">${spanish}</option>
                      <option value="french">${french}</option>
                      <option value="history_of_belarus">${history_of_belarus}</option>
                      <option value="world_history">${world_history}</option>
                      <option value="civics">${civics}</option>
                      <option value="geography">${geography}</option>
                    </select> ${score}
                    <input type="text" name="test2" id="subject_mark" required="" placeholder="100">
                </span>
    </div>
    <div>
                <span>${enter} ${subject}:
                    <select name="subject3" id="subject">
                      <option value="russian_lang">${russian_lang}</option>
                      <option value="belorusian_lang">${belarusian_lang}</option>
                      <option value="math">${math}</option>
                      <option value="physics">${physics}</option>
                      <option value="chemistry">${chemistry}</option>
                      <option value="biology">${biology}</option>
                      <option value="german">${german}</option>
                      <option value="english">${english}</option>
                      <option value="spanish">${spanish}</option>
                      <option value="french">${french}</option>
                      <option value="history_of_belarus">${history_of_belarus}</option>
                      <option value="world_history">${world_history}</option>
                      <option value="civics">${civics}</option>
                      <option value="geography">${geography}</option>
                    </select> ${score}
                    <input type="text" name="test3" id="subject_mark" required="" placeholder="100">
                </span>
    </div>
    <input type="submit" name="registration" value="${register}"/>
  </form>

  <c:if test="${requestScope.error != null}">
    <div class="error">
      <p>${message}</p>
        <%-- <p>${requestScope.error}</p>--%>
    </div>
  </c:if>

</div>

<mytag:copyright/>


<script>
  var select = document.querySelector("#day");
  for(var i=1; i<10; i++){
    select.innerHTML += "<option value=0" + i + ">0"+i+"</option>";
  }
  for(var i=10; i<=31; i++){
    select.innerHTML += "<option value=" + i + ">"+i+"</option>";
  }

  var select = document.querySelector("#month");
  for(var i=1; i<10; i++){
    select.innerHTML += "<option value=0" + i + ">0"+i+"</option>";
  }
  for(var i=10; i<=12; i++){
    select.innerHTML += "<option value=" + i + ">"+i+"</option>";
  }

  var select = document.querySelector("#year");
  for(var i=2000; i>1915; i--){
    select.innerHTML += "<option value=" + i + ">"+i+"</option>";
  }

</script>

</body>
</html>
