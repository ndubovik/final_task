
<%--
  Created by IntelliJ IDEA.
  User: Настенька
  Date: 11/23/2015
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/taglib.tld" %>
<html>
<head>
  <fmt:setLocale value="${sessionScope.locale}"/>
  <fmt:setBundle basename="localize" var="loc"/>
  <fmt:message bundle="${loc}" key="local.title.form" var="title"/>
  <fmt:message bundle="${loc}" key="local.title.main" var="main"/>
  <fmt:message bundle="${loc}" key="local.title.mail" var="mail"/>
  <fmt:message bundle="${loc}" key="local.title.apply" var="apply"/>
  <fmt:message bundle="${loc}" key="local.login" var="login"/>
  <fmt:message bundle="${loc}" key="local.button.logout" var="logout"/>
  <fmt:message bundle="${loc}" key="local.password" var="password"/>
  <fmt:message bundle="${loc}" key="local.register" var="register"/>
  <fmt:message bundle="${loc}" key="local.ru" var="ru"/>
  <fmt:message bundle="${loc}" key="local.en" var="en"/>
  <fmt:message bundle="${loc}" key="local.error.message" var="message"/>
  <fmt:message bundle="${loc}" key="local.button.goback" var="goback"/>
  <fmt:message bundle="${loc}" key="local.enter" var="enter"/>

  <fmt:message bundle="${loc}" key="local.registration.fio" var="fio"/>
  <fmt:message bundle="${loc}" key="local.registration.first_name" var="first_name"/>
  <fmt:message bundle="${loc}" key="local.registration.last_name" var="last_name"/>
  <fmt:message bundle="${loc}" key="local.registration.patronymic" var="patronymic"/>
  <fmt:message bundle="${loc}" key="local.registration.date_of_birth" var="date_of_birth"/>
  <fmt:message bundle="${loc}" key="local.registration.day" var="day"/>
  <fmt:message bundle="${loc}" key="local.registration.month" var="month"/>
  <fmt:message bundle="${loc}" key="local.registration.year" var="year"/>
  <fmt:message bundle="${loc}" key="local.registration.passport_data" var="passport_data"/>
  <fmt:message bundle="${loc}" key="local.registration.adress" var="adress"/>
  <fmt:message bundle="${loc}" key="local.registration.city" var="city"/>
  <fmt:message bundle="${loc}" key="local.registration.street" var="street"/>
  <fmt:message bundle="${loc}" key="local.registration.house" var="house"/>
  <fmt:message bundle="${loc}" key="local.registration.flat" var="flat"/>
  <fmt:message bundle="${loc}" key="local.registration.school_score" var="school_score"/>
  <fmt:message bundle="${loc}" key="local.registration.subject" var="subject"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.russian_lang" var="Russian_Language"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.belarusian_lang" var="Belarusian_Language"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.math" var="Math"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.physics" var="Physics"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.chemistry" var="Chemistry"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.biology" var="Biology"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.german" var="German"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.english" var="English"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.spanish" var="Spanish"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.french" var="French"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.history_of_belarus" var="History_Of_Belarus"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.world_history" var="World_History"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.civics" var="Civics"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.geography" var="Geography"/>
  <fmt:message bundle="${loc}" key="local.registration.subject.score" var="score"/>

  <fmt:message bundle="${loc}" key="local.main.phrase" var="phrase"/>

  <fmt:message bundle="${loc}" key="local.form.edit" var="edit"/>

  <title>${title}</title>
  <link rel="stylesheet" href="css/form/style2.css" type="text/css" />

</head>
<body>

<header>
  <div>
    <p>${sessionScope.get("user").getLogin()}</p>
    <form action="Controller" method="post">
      <input type="hidden" name="action" value="logout"/>
      <input type="submit" value="${logout}"/>
    </form>
    <nav>
      <ul>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="main"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${main}"/>
          </form>
        </li>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="form"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${title}"/>
          </form>
        </li>
        <li>
          <form action="Controller" method="post">
            <input type="hidden" name="action" value="mail"/>
            <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
            <input type="submit" value="${mail}"/>
          </form>
        </li>
      </ul>
    </nav>
  </div>
</header>

<div class="content">

  <div class="comment">
    <img src="img/comment.png"/>
    <p>${phrase}</p>
  </div>

  <section>

    <div class="main">
      <h1>${title}</h1>
      <div>
        ${login}: ${sessionScope.get("user").getLogin()}
      </div>
      <div>
        ${fio}: ${requestScope.abiturient.getLastName()} ${requestScope.abiturient.getFirstName()} ${requestScope.abiturient.getPatronymic()}
      </div>
      <div>
        ${passport_data}: ${requestScope.abiturient.getPassportData()}
      </div>
      <div>
        ${date_of_birth}: ${requestScope.abiturient.getBirthYear()}-${requestScope.abiturient.getBirthMonth()}-${requestScope.abiturient.getBirthDay()}
      </div>
      <div>
        ${adress}: ${requestScope.abiturient.getCity()} ${requestScope.abiturient.getStreet()} ${requestScope.abiturient.getHouse()} ${requestScope.abiturient.getFlat()}
      </div>
      <div>
        ${school_score}: ${requestScope.abiturient.getSchoolScore()}
      </div>

      <c:forEach var="test" items="${requestScope.abiturient.getTests()}">
        <div>

          <c:if test="${sessionScope.locale == null }">
            ${test.getSubject().getEnglishName()}: ${test.getScore()}
          </c:if>

          <c:if test="${sessionScope.locale.equals('en')}">
            ${test.getSubject().getEnglishName()}: ${test.getScore()}
          </c:if>

          <c:if test="${sessionScope.locale.equals('ru')}">
            ${test.getSubject().getRussianName()}: ${test.getScore()}
          </c:if>

        </div>
      </c:forEach>


    </div>

    <c:if test="${requestScope.abiturient.isAvailable() == true}">
      <form action="Controller" method="post">
        <input type="hidden" name="action" value="go_to_edit_form"/>
        <input type="hidden" name="page" value="${pageContext.request.requestURI}"/>
        <input type="submit" value="${edit}"/>
      </form>
    </c:if>

  </section>

</div>

<mytag:copyright/>

</body>
</html>
